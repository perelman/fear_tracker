"""
ASGI entrypoint. Configures Django and then runs the application
defined in the ASGI_APPLICATION setting.
"""

import django

import fear_tracker.routing

django.setup()
application = fear_tracker.routing.application
