"""fear_tracker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('join/', views.enter_code, name='enter_code'),
    path('new/', views.new_game, name='new_game'),
    url(r'^(?P<access_code>[a-zA-Z]{6})/', include([
        path('', views.game, name='game'),
        path('update/', views.update_game, name='update_game'),
        path('qr/', views.qr_code, name='qr_code'),
        path('status/', views.status, name='status'),
    ])),
]
