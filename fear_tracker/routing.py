from channels.routing import ProtocolTypeRouter, URLRouter

import django
from django.conf.urls import url
from django.core.asgi import get_asgi_application

# TODO Importing views breaks if django.setup() isn't called first...
#      ... but this isn't the way this is supposed to work.
django.setup()

from . import views

application = ProtocolTypeRouter({
    "http": URLRouter([
        url(r'^(?P<access_code>[a-zA-Z]{6})/', URLRouter([
            url(r"status/(?P<hashcode>[a-z0-9]{64})/",
                views.StatusLongPollConsumer.as_asgi(), name='status'),
        ])),
        url(r"", get_asgi_application()),
    ]),
})
