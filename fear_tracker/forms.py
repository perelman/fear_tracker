from django import forms

from .models import Game, Player


class PlayerForm(forms.Form):
    name = forms.CharField(max_length=80, label="Name", required=False)
    spirit = forms.TypedChoiceField(label="Spirit",
                                    choices=Player.enumerate_spirit_names(),
                                    empty_value=None, coerce=int)


class BasePlayerFormSet(forms.BaseFormSet):
    def clean(self):
        if any(self.errors):
            return
        names = set()
        for form in self.forms:
            if self.can_delete and self._should_delete_form(form):
                continue
            name = form.cleaned_data.get('name')
            if name:
                if name in names:
                    raise forms.ValidationError(
                            "Players must have distinct names.")
                names.add(name)
        if not names:
            raise forms.ValidationError("Must have at least one player.")


PlayerFormSet = forms.formset_factory(PlayerForm, formset=BasePlayerFormSet)


class NewGameForm(forms.Form):
    combined_growth_spirit = forms.BooleanField(
            required=False, initial=True,
            label="Combine Growth and Spirit phases into a single phase")
    enable_events = forms.BooleanField(
            required=False, initial=False,
            label="Enable event phase (Branch and Claw expansion)")
    combined_event = forms.BooleanField(
            required=False, initial=False,
            label="Combine three event phase steps into one phase")
    england_build = forms.BooleanField(
            required=False, initial=False,
            label="High Immigration (extra build phase for England level 3+)")
    fear_per_player = forms.IntegerField(
            label="Fear per player", initial=4, min_value=1, max_value=99)


class JoinGameForm(forms.Form):
    game = forms.CharField(label='Access code',
                           max_length=Game.ACCESS_CODE_LENGTH)

    def clean_game(self):
        data = self.cleaned_data['game']

        try:
            return Game.objects.get(access_code=data.lower())
        except Game.DoesNotExist:
            raise forms.ValidationError("Invalid access code.")
