# fear-tracker

Helper for tracking the status of the board game
[Spirit Island](https://boardgamegeek.com/boardgame/162886/spirit-island).
Allows multiple players to announce fear and phase status via a web
site to reduce opportunities for confusion and collect more detailed
statistics on the game. Mainly intended for larger (>4 player) games but
may also be useful in smaller games.
